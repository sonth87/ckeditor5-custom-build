/**
 * @license Copyright (c) 2003-2023, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
// import ClassicEditorBase from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";

import { ClassicEditor as ClassicEditorBase } from "@ckeditor/ckeditor5-editor-classic";
import { Alignment } from "@ckeditor/ckeditor5-alignment";
import { Autoformat } from "@ckeditor/ckeditor5-autoformat";
import {
  Bold,
  Code,
  Italic,
  Strikethrough,
  Subscript,
  Superscript,
  Underline,
} from "@ckeditor/ckeditor5-basic-styles";
import { BlockQuote } from "@ckeditor/ckeditor5-block-quote";
import { CloudServices } from "@ckeditor/ckeditor5-cloud-services";
import { CodeBlock } from "@ckeditor/ckeditor5-code-block";
import { Essentials } from "@ckeditor/ckeditor5-essentials";
import { FindAndReplace } from "@ckeditor/ckeditor5-find-and-replace";
import {
  FontBackgroundColor,
  FontColor,
  FontSize,
} from "@ckeditor/ckeditor5-font";
import { Heading } from "@ckeditor/ckeditor5-heading";
import { Highlight } from "@ckeditor/ckeditor5-highlight";
import { HorizontalLine } from "@ckeditor/ckeditor5-horizontal-line";
import { GeneralHtmlSupport } from "@ckeditor/ckeditor5-html-support";
import {
  AutoImage,
  Image,
  ImageCaption,
  ImageInsert,
  ImageResize,
  ImageStyle,
  ImageToolbar,
  ImageUpload,
} from "@ckeditor/ckeditor5-image";
import { Indent, IndentBlock } from "@ckeditor/ckeditor5-indent";
import { AutoLink, Link, LinkImage } from "@ckeditor/ckeditor5-link";
import { List, ListProperties, TodoList } from "@ckeditor/ckeditor5-list";
import { MediaEmbed, MediaEmbedToolbar } from "@ckeditor/ckeditor5-media-embed";
import { PageBreak } from "@ckeditor/ckeditor5-page-break";
import { Paragraph } from "@ckeditor/ckeditor5-paragraph";
import { PasteFromOffice } from "@ckeditor/ckeditor5-paste-from-office";
import { StandardEditingMode } from "@ckeditor/ckeditor5-restricted-editing";
import { SelectAll } from "@ckeditor/ckeditor5-select-all";
import { SourceEditing } from "@ckeditor/ckeditor5-source-editing";
import { Style } from "@ckeditor/ckeditor5-style";
import {
  Table,
  TableCaption,
  TableCellProperties,
  TableColumnResize,
  TableProperties,
  TableToolbar,
} from "@ckeditor/ckeditor5-table";
import { TextTransformation } from "@ckeditor/ckeditor5-typing";
import { SimpleUploadAdapter } from "@ckeditor/ckeditor5-upload";
import { WordCount } from "@ckeditor/ckeditor5-word-count";

export default class ClassicEditor extends ClassicEditorBase {
  static builtinPlugins: (
    | typeof Alignment
    | typeof AutoImage
    | typeof AutoLink
    | typeof Autoformat
    | typeof BlockQuote
    | typeof Bold
    | typeof CloudServices
    | typeof Code
    | typeof CodeBlock
    | typeof Essentials
    | typeof FindAndReplace
    | typeof FontBackgroundColor
    | typeof FontColor
    | typeof FontSize
    | typeof GeneralHtmlSupport
    | typeof Heading
    | typeof Highlight
    | typeof HorizontalLine
    | typeof Image
    | typeof ImageCaption
    | typeof ImageInsert
    | typeof ImageResize
    | typeof ImageStyle
    | typeof ImageToolbar
    | typeof ImageUpload
    | typeof Indent
    | typeof IndentBlock
    | typeof Italic
    | typeof Link
    | typeof LinkImage
    | typeof List
    | typeof ListProperties
    | typeof MediaEmbed
    | typeof MediaEmbedToolbar
    | typeof PageBreak
    | typeof Paragraph
    | typeof PasteFromOffice
    | typeof SelectAll
    | typeof SimpleUploadAdapter
    | typeof SourceEditing
    | typeof StandardEditingMode
    | typeof Strikethrough
    | typeof Style
    | typeof Subscript
    | typeof Superscript
    | typeof Table
    | typeof TableCaption
    | typeof TableCellProperties
    | typeof TableColumnResize
    | typeof TableProperties
    | typeof TableToolbar
    | typeof TextTransformation
    | typeof TodoList
    | typeof Underline
    | typeof WordCount
  )[];
  static defaultConfig: {
    toolbar: {
      items: string[];
    };
    language: string;
    image: {
      toolbar: string[];
    };
    table: {
      contentToolbar: string[];
    };
    htmlSupport: any;
  };
}

declare module "@ckeditor/ckeditor5-custom-build" {
  const ClassicEditor: any;

  export = ClassicEditor;
}
